import pickle
import os
import time
import openravepy
import numpy.random as rand
if not __openravepy_build_doc__:
    from openravepy import *
import sys
import os
import numpy as np
from optparse import OptionParser
from openravepy.misc import OpenRAVEGlobalArguments
import matplotlib.image as mpimg
import matplotlib.pyplot as plt


global home
home = os.path.expanduser('~')

def main(env,options):
	img=mpimg.imread('zz.png')
	M = len(img)
	N = len(img[0])
	X = 4.0
	Y = 4.0
	xlen = X*0.5/N
	ylen = Y*0.5/M
	kinbodies = []
	for i in range(M):
		for j in range(N):
			xcenter = (j+1)*2.0*xlen - xlen
			ycenter = (i+1)*2.0*ylen - ylen
			kinbodies.append(RaveCreateKinBody(env,''))
			kinbodies[-1].InitFromBoxes(numpy.array([[xcenter,ycenter,-0.0001,xlen,ylen,0.0001]]),True)
			kiname = str(i)+"-"+str(j)
			kinbodies[-1].SetName(kiname)
			env.AddKinBody(kinbodies[-1],True)
			kinbodies[-1].GetLinks()[0].GetGeometries()[0].SetDiffuseColor(list(img[i,j])[:-1])
	env.Save('1.dae',Environment.SelectionOptions.Everything)
	raw_input('done')

@openravepy.with_destroy
def run(args=None):
    global env
    parser = OptionParser(description='Explicitly specify goals to get a simple navigation and manipulation demo.', usage='openrave.py --example hanoi [options]')
    OpenRAVEGlobalArguments.addOptions(parser)
    parser.add_option('--planner',action="store",type='string',dest='planner',default=None,help='the planner to use')
    (options, leftargs) = parser.parse_args(args=args)
    env = OpenRAVEGlobalArguments.parseAndCreate(options,defaultviewer=True)
    main(env,options)
					    
if __name__ == "__main__":
	run()
