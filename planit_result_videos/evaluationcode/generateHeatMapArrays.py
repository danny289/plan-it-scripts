import os
import sys
import numpy as np
sys.path.append('/home/ashesh/project/robotics/learning_affordances/')
import matplotlib.pyplot as plt 
import plotheatmap as phm
import copy
import Image

global home
home = os.path.expanduser('~')

def main():
	#path = home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/environment/'
	path = '../environment/'
	env_list = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	use_beta = True
	for e in env_list:
		env_path = path + 'env_{0}_context_{1}.dae'.format(e,1)
		context_graph_path = path + '{0}_graph_{1}.xml'.format(e,1)
		if e <= 8:
			params_file_path = path + '../params/params_task_bedroom_env_1,2,3,4,5,6,7,8,9_context_1,2,3,4_s_False_r_False_n_False_b_True.pik'
		else:
			params_file_path = path + '../params/params_task_entertainment_env_1,2,3,4,5,6,7,8_context_1,2,3,4_s_False_r_False_n_False_b_True.pik'
		if os.path.exists(env_path) and os.path.exists(context_graph_path) and os.path.exists(params_file_path): 
			heatmap_array,maxS= phm.heatmap(env_path,context_graph_path,params_file_path,use_beta)		
			print "Generating envArray: "+str(e)
			print "Max score :"+str(maxS)
			'''for row in range (0,len(heatmap_array)):		#80x80
				for col in range (0,len(heatmap_array[0])):
					heatmap_array[row][col]= (heatmap_array[row][col]/totalSum)*100'''
			np.save("env"+str(e)+".npy", heatmap_array)

if __name__=="__main__":
	main()
