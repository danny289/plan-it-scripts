from __future__ import with_statement # for python 2.5
__author__= 'Ashesh Jain'
import os
import time
import openravepy
import numpy.random as rand
if not __openravepy_build_doc__:
    from openravepy import *
    from numpy import *
import xml.dom.minidom
import sys
sys.path.append('../evaluationcode/')
import plotheatmap as pm
import os
import numpy as np
from optparse import OptionParser
from openravepy.misc import OpenRAVEGlobalArguments

def waitrobot(robot):
	"""busy wait for robot completion"""
	while not robot.GetController().IsDone():
		time.sleep(0.01)

def main(env,options):
	env_colladafile = sys.argv[1]
	list_traj = planmanytraj(env_colladafile,env)
	
def filler():
	'''
	This filler method is only for testing purpose. You may ignore it. 
	'''
	env_colladafile = '../environment/env_2_context_1.dae'
	context_graph = '../environment/2_graph_1.xml'
	params_file = '../params/params_task_bedroom_env_1,2,3,4,5,6,7,8,9_context_1,2,3,4_s_False_r_False_n_False_b_True.pik'
	FindBestTraj(env_colladafile,context_graph,params_file)

'''
Mostly this will be the entering function.
'''
def FindBestTraj(env_colladafile,context_graph,params_file):
	'''
	Input: Environment colladafile, context graph file and learned params pickle file
	Output: Executes top k trajectories
	Description: It samples many trajectories using planmanytraj() function and ranks
	them using the learned params.
	'''
	scores = []
	list_traj,env = planmanytraj(env_colladafile)
	list_traj = np.array(list_traj)
	for traj in list_traj:
		score = pm.scoresingletraj(env_colladafile,context_graph,params_file,traj,use_beta=True)
		scores.append(score)
	scores = np.array(scores)
	sorted_args = np.argsort(scores)
	sorted_list_traj = list_traj[sorted_args]
	sorted_scores = scores[sorted_args]
	print "Scores"
	print sorted_scores
	executeTopKtraj(env,env_colladafile,sorted_list_traj,3)
	env.Destroy()

def executeTopKtraj(env,env_colladafile,list_traj,k=3):
	'''
	Input: Environment colladafile and a list of trajectories
	Output: It executes top k trajectories from the list
	Description: It first loads the environment from colladafile and then execute the trajectories.
	'''
	k = min([k,len(list_traj)])
	executeAllTraj(env,list_traj[:k])

def planmanytraj(env_colladafile,env=None):
	'''
	Input: An environment pointer and path to the environment dae file to be loaded
	Output: Many diverse trajectories
	Description: It ask user for robot start and end configuration and then plans 
	many diverse trajectories between the two configurations. To produce diverse 
	trajectories it plans a trajectory and then updates the environment by blocking 
	the trajectory by introducing an artificial obstacle. This forces next trajectory
	to be different.
	'''
	global obstacle_count
	obstacle_count = 0
	if env is None:
		env = Environment()		
		env.SetViewer('qtcoin')
	env.Load(env_colladafile)
	robot = env.GetRobots()[0]
	raw_input('Enter start config')
	start_config = robot.GetTransform()
	raw_input('Enter end config')
	end_config = robot.GetTransform()
	
	list_traj = []
	for fact in [0.5]:
		with env:
			env.UpdatePublishedBodies()	    	
		for i in range(100):
			traj = plantraj(env,start_config,end_config)
			if traj is not None:
				list_traj.append(traj)
				env = addObstacle(env,traj,fact)
			else:
				env = deleteAllObstacles(env)
				break
	return list_traj,env

def deleteAllObstacles(env):
	'''
	Description: This function deletes all the obstacles
	'''
	for body in env.GetBodies():
		kinname = body.GetName()
		first_name = kinname.split('-')[0]
		if first_name == 'obstacle':
			with env:
				env.Remove(body)
	with env:
		env.UpdatePublishedBodies()	    	
	return env
		
def executeAllTraj(env,list_traj):
	'''
	Input: An environment pointer and a list of trajectories
	Description: This functions plays the trajectories in the list
	'''
	with env:
		env.UpdatePublishedBodies()	    	
	robot = env.GetRobots()[0]
	for openrave_traj in list_traj:
		traj = RaveCreateTrajectory(env,'')
		traj.deserialize(openrave_traj)
		robot.GetController().SetPath(traj)
		waitrobot(robot) 

def plantraj(env,start_config,end_config):
	'''
	Input: An environment pointer, robot start and end confguration matrices.
	Output: A trajectory in the environment.
	'''
	print "next traj"
	robot = env.GetRobots()[0]
	#ikmodel = databases.inversekinematics.InverseKinematicsModel(robot,iktype=IkParameterization.Type.Transform6D)
	#if not ikmodel.load():
	#	ikmodel.autogenerate()
	plannertype = "BiRRT"
	basemanip = interfaces.BaseManipulation(robot,plannername=plannertype)
        
	Tgoal_ = end_config
        angx = math.acos(Tgoal_[0][0])
        angy = math.acos(Tgoal_[1][0])
	if angy < 0.5*math.pi:
		ang = angx
        else:
	        ang = 2*math.pi - angx
	with env:
		robot.SetTransform(start_config)
	try:
		with env:
			robot.SetActiveDOFs([],DOFAffine.X|DOFAffine.Y|DOFAffine.RotationAxis,[0,0,1])
			traj = basemanip.MoveActiveJoints(goal=[Tgoal_[0][3],Tgoal_[1][3],ang],maxiter=10000,steplength=0.15,maxtries=2,outputtraj=True,execute=False)
		waitrobot(robot)
		planned_successfully = True
	except planning_error,e:
		print e
		traj = None
		planned_successfully = False
	return traj


def addObstacle(env,traj,fact):
	'''
	Input: environment pointer, a trajectory and a factor
	Output: environment
	Description: This function puts a tall obstacle at fact*trajectory_duration
	and returns the new environment. 
	'''
	global obstacle_count
        trajptr = RaveCreateTrajectory(Environment(),'')
        trajptr.deserialize(traj)
	traj_duration = trajptr.GetDuration()
	midwaypoint = trajptr.Sample(fact*traj_duration)

	with env:
		obstacle = RaveCreateKinBody(env,'')
		obstacle.SetName('obstacle-'+str(obstacle_count))
		obstacle.InitFromBoxes(numpy.array([[midwaypoint[0], midwaypoint[1], 0.1 ,0.01,0.01,0.1]]),True)
		env.Add(obstacle,True)
		obstacle_count += 1
		env.UpdatePublishedBodies()	    	
	return env

@openravepy.with_destroy
def run(args=None):
	global env
	parser = OptionParser(description='Explicitly specify goals to get a simple navigation and manipulation demo.', usage='openrave.py --example hanoi [options]')
	OpenRAVEGlobalArguments.addOptions(parser)
	parser.add_option('--planner',action="store",type='string',dest='planner',default=None,help='the planner to use')
	(options, leftargs) = parser.parse_args(args=args)
	env = OpenRAVEGlobalArguments.parseAndCreate(options,defaultviewer=True)
	main(env,options)
	time.sleep(4)

if __name__ == "__main__":
	filler()
