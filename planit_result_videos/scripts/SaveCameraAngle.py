import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import scipy
import pickle
import os
import sys
import time
import openravepy
import numpy.random as rand
if not __openravepy_build_doc__:
    from openravepy import *
    from numpy import *
import numpy as np
import RecordTrajectory as rec
from optparse import OptionParser
from openravepy.misc import OpenRAVEGlobalArguments

global home
home = os.path.expanduser('~')

def main(env,options):
	path = '../environment/' #home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/environment/'
	env_list = range(1,21)

	if len(sys.argv) == 2:
		env_list = [int(sys.argv[1])]

	for e in env_list:
		env_path = path + 'env_{0}_context_{1}.dae'.format(e,1)
		if not os.path.exists(env_path):
			continue
		#print "########Environment={0}############".format(e)
		env.Load(env_path)
		camera_angle_file = path + 'camera_angle_env_{0}_context_{1}.pik'.format(e,1)
		camera_image_file = path + 'camera_image_env_{0}_context_{1}.jpg'.format(e,1)
		viewer = env.GetViewer()
		#tt = np.array([[1,0,0,0],[0,-1,0,0],[0,0,-1,0],[0,0,0,1]])
		file_exists = '1'
		if os.path.exists(camera_angle_file):
			colorhumans(env.GetBodies(),False)
			#colorfloorandwall(e)
			with open(camera_angle_file,'rb') as ff:
				cam_params = pickle.load(ff)
			viewer.SetCamera(cam_params)
			time.sleep(1)
			I = env.GetViewer().GetCameraImage(640,480,env.GetViewer().GetCameraTransform(),[640,640,320,240])
			scipy.misc.imsave(camera_image_file,I)
			file_exists = raw_input("Camera angle file already exists. Press 0 to skip and 1 to continue\n")

		if not file_exists == '0':
			traj_path = path + 'traj_env_{0}_context_{1}_id_1.pik'.format(e,1)
			colorhumans(env.GetBodies(),False)
			#colorfloorandwall(e)
			rec.Playtraj(traj_path,env)
			inp = raw_input('Set view angle. Press 1 to save and 0 to skip\n')
			if inp == '1':
				with open(camera_angle_file,'wb') as ff:
					pickle.dump(viewer.GetCameraTransform(),ff,-1)
				I = env.GetViewer().GetCameraImage(640,480,env.GetViewer().GetCameraTransform(),[640,640,320,240])
				scipy.misc.imsave(camera_image_file,I)
		deleteallbodies(env.GetBodies())



def deleteallbodies(bodies):
	for body in bodies:
		with env:
			env.RemoveKinBody(body)

def colorfloorandwall(env_num):
	#home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/environment/master_color'
	color_file = open('../environment/master_color','r')
	lines = color_file.readlines()
	line = lines[env_num-1].strip().split(';')
	color_val = line[1]
	wall_color = [float(x) for x in color_val.split(',')]
	wallnames = ['Wall-1','Wall-2','Wall-3','Wall-4']
        floor_color = [0.5,0.5,0.5]     
	if int(env_num) > 15:
		wallnames = ['wall-1','wall-2','wall-3','wall-4']
		env.GetKinBody('floor').GetLinks()[0].GetGeometries()[0].SetDiffuseColor(floor_color)
	else:
		env.GetKinBody('Floor').GetLinks()[0].GetGeometries()[0].SetDiffuseColor(floor_color)
        for walls in wallnames:
		env.GetKinBody(walls).GetLinks()[0].GetGeometries()[0].SetDiffuseColor(wall_color)



def colorhumans(bodies,removeRobot=True):
	colors = [(1.0,0.0,0.0), (0.0,1.0,0.0), (1.0,0.5,0.1), (1.0,1.0,0.0), (1.0,10.2,0.8), (0.0,1.0,1.0), (0.5,0.2,0.5)]
	count = 0
	robot = env.GetRobots()[0]
	if removeRobot:
		env.RemoveKinBody(robot)
	for body in bodies:	
		if body.GetName().startswith('human'):
			body.GetLinks()[0].GetGeometries()[0].SetDiffuseColor(colors[count])
			count += 1

@openravepy.with_destroy
def run(args=None):
    global env
    parser = OptionParser(description='Explicitly specify goals to get a simple navigation and manipulation demo.', usage='openrave.py --example hanoi [options]')
    OpenRAVEGlobalArguments.addOptions(parser)
    parser.add_option('--planner',action="store",type='string',dest='planner',default=None,help='the planner to use')
    (options, leftargs) = parser.parse_args(args=args)
    env = OpenRAVEGlobalArguments.parseAndCreate(options,defaultviewer=True)
    main(env,options)
					    
if __name__ == "__main__":
	run()
