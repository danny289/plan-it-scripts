"""Alternative planner to a.py; Connects with Plan-It rails server
"""
from __future__ import with_statement # for python 2.5
__author__= 'Ashesh Jain, Daniel Liu'
import os
import time
import openravepy
import numpy.random as rand
if not __openravepy_build_doc__:
    from openravepy import *
    from numpy import *
import xml.dom.minidom
import sys
sys.path.append('../evaluationcode/')
import plotheatmap as pm
import os
import numpy as np
from optparse import OptionParser
from openravepy.misc import OpenRAVEGlobalArguments
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import urlparse

def waitrobot(robot):
	"""busy wait for robot completion"""
	while not robot.GetController().IsDone():
		time.sleep(0.01)
def plantraj(env,start_config,end_config):
	'''
	Input: An environment pointer, robot start and end confguration matrices.
	Output: A trajectory in the environment.
	'''
	print "Planning traj"
	robot = env.GetRobots()[0]
	#ikmodel = databases.inversekinematics.InverseKinematicsModel(robot,iktype=IkParameterization.Type.Transform6D)
	#if not ikmodel.load():
	#	ikmodel.autogenerate()
	plannertype = "birrt"
	basemanip = interfaces.BaseManipulation(robot,plannername=plannertype)
        
	Tgoal_ = end_config
        angx = math.acos(Tgoal_[0][0])
        angy = math.acos(Tgoal_[1][0])
	if angy < 0.5*math.pi:
		ang = angx
        else:
	        ang = 2*math.pi - angx
	with env:
		robot.SetTransform(start_config)
	try:
		with env:
			robot.SetActiveDOFs([],DOFAffine.X|DOFAffine.Y|DOFAffine.RotationAxis,[0,0,1])
			traj = basemanip.MoveActiveJoints(goal=[Tgoal_[0][3],Tgoal_[1][3],ang],maxiter=10000,steplength=0.15,maxtries=1,outputtraj=True,execute=False)
		waitrobot(robot)
		planned_successfully = True
	except planning_error,e:
		print e
		traj = None
		planned_successfully = False
	return traj, planned_successfully


#xyDim is robot env dim, imageXY is image dim,  x1,y1 is pixel coord
def PixelToRobot(xdim,ydim, x1, y1, imageX, imageY):	
	M = imageX #image pixel size
	N = imageY #image pixel size
	X = 2.0*xdim
	Y = 2.0*ydim
	xlen = X*0.5/N
	ylen = Y*0.5/M
	xcenter = (x1+1)*2.0*xlen - xlen - (0.5*X)   #Robot coordinates
	ycenter = (0.5*Y) - ( (y1+1)*2.0*ylen - ylen)
	return xcenter, ycenter

#Only calc xlen,ylen
def xylen(xdim,ydim,imageX, imageY):
	M = imageX #image pixel size
	N = imageY #image pixel size
	X = 2.0*xdim
	Y = 2.0*ydim
	xlen = X*0.5/N
	ylen = Y*0.5/M
	return xlen, ylen

def sampleTraj(env,trajInput):
	'''
	Input: An environment pointer and a list of trajectories
	Description: This functions plays the trajectories in the list
	'''
	print 'Sampling Traj'
	with env:
		env.UpdatePublishedBodies()	    	
	listSamples=[]
	traj = RaveCreateTrajectory(env,'')
	traj.deserialize(trajInput)

	traj_duration = traj.GetDuration()
	x=0
	while (x<traj_duration):
		trajdata=traj.Sample(x)
		tempTuple=(trajdata[0], trajdata[1], trajdata[2])
		listSamples.append(tempTuple)
		x+=1
	return listSamples

def addObstacle(env,x,y):
	'''
	adds an Obstacle at the x,y location; Does Not UpdatePublishedBodies
	'''
	global obstacle_count
	with env:
		obstacle = RaveCreateKinBody(env,'')
		obstacle.SetName('obstacle-'+str(obstacle_count))
		obstacle.InitFromBoxes(numpy.array([[x, y, 0.1 ,0.10,0.10,0.1]]),True)
		env.Add(obstacle,True)
		obstacle_count += 1	  

def deleteAllObstacles(env):
	'''
	Description: This function deletes all the obstacles
	'''
	global obstacle_count
	for body in env.GetBodies():
		kinname = body.GetName()
		first_name = kinname.split('-')[0]
		if first_name == 'obstacle':
			with env:
				env.Remove(body)
	with env:
		env.UpdatePublishedBodies()	    
	obstacle_count=0	

#Heatmap Array is 80x80; Image on web is 308x308
def main(envNumIn,coordList):
	envNum=envNumIn
	env_colladafile = '../environment/env_'+envNum+'_context_1.dae'
	env = Environment() # create the environment
	#env.SetViewer('qtcoin') # start the viewer
	env.Load(env_colladafile)
	robot = env.GetRobots()[0] # get the first robot
	global obstacle_count
	obstacle_count=0
	env.UpdatePublishedBodies()
	time.sleep(0.1)

	manipprob = interfaces.BaseManipulation(robot) # create the interface for basic manipulation programs
	with env:
		robot.SetActiveDOFs([],DOFAffine.X|DOFAffine.Y|DOFAffine.RotationAxis,[0,0,1]) #moves the base
		w1 = env.GetKinBody('Wall-1')
		w3 = env.GetKinBody('Wall-3')
		xdim = w3.GetLinks()[0].GetGeometries()[0].GetBoxExtents()[0]
		ydim = w1.GetLinks()[0].GetGeometries()[0].GetBoxExtents()[1]

		j=coordList[0]  #x
		i=coordList[1]  #y
		xcenter,ycenter=PixelToRobot(xdim,ydim,j,i,308,308)

		j=coordList[2]  #x
		i=coordList[3]  #y
		xcenter2,ycenter2=PixelToRobot(xdim,ydim,j,i,308,308)

		start_config=[[0,1,0,xcenter],[-1,0,-0,ycenter],[-0,0,1,0.04526],[0,0,0,1]]
		end_config=[[0,1,0,xcenter2],[-1,0,-0,ycenter2],[-0,0,1,0.04526],[0,0,0,1]]
	
		#Check Start/Goal Collisions
		with robot:
			robot.SetActiveDOFs([],DOFAffine.X|DOFAffine.Y|DOFAffine.RotationAxis,[0,0,1]) #moves the base
			robot.SetActiveDOFValues([xcenter,ycenter,0])
			if env.CheckCollision(robot):
				print "error_CollisionStart"
				pixelTraj="error_Collision"
				return pixelTraj,env
			robot.SetActiveDOFValues([xcenter2,ycenter2,0])
			if env.CheckCollision(robot):
				print "error_CollisionGoal"
				pixelTraj="error_Collision"
				return pixelTraj,env
		
		#Add Obstacles
		maxThreshEnv=[0.468130093932,0.625690434246,0.697627020728,1.39128673944,1.2367608488,0.411941447636,1.2192291683,0.60350522026,0.392524259399,1.37457149852,0.512757512297,0.496851952358,1.25210994328,0.282127061375,0.248282652797]
		maxThresh=maxThreshEnv[int(envNum)-1]
		lowThresh=maxThresh/4

		array=np.load("../evaluationcode/env"+envNum+".npy")
		print "adding objects"
		for row in range(0,80,4):
			for col in range(0,80,4):
				if (array[row][col]>lowThresh):
					x,y=PixelToRobot(xdim,ydim,col,row,80,80)
					addObstacle(env,x,y)	    
		env.UpdatePublishedBodies()	
		print "finished updating: " +str(obstacle_count)

		#Plan Traj; Reset threshold if necessary
		success=False
		step=(maxThresh-lowThresh)/3	
		for redo in range (1,5):
			if (obstacle_count<50):
				traj, success = plantraj(env,start_config,end_config)
			if (success):
				break
			deleteAllObstacles(env)
			print "resetting threshold; redo: "+str(redo)
			for row in range(0,80,4):
				for col in range(0,80,4):
					if (array[row][col]>(lowThresh+redo*step)):
						x,y=PixelToRobot(xdim,ydim,col,row,80,80)
						addObstacle(env,x,y)	    
			print "finished updating: " +str(obstacle_count)
	
		#Sample Trajectory and convert to pixel coodinates
		pixelTraj=""
		if (success):
			listSamples=sampleTraj(env,traj)
			xlen,ylen=xylen(xdim,ydim,308, 308)
			x=(xlen*308)/0.5
			y=(ylen*308)/0.5
			for i in range (len(listSamples)):
				tempX= int( ( (listSamples[i])[0] +xlen +(0.5*x) )/ (2.0*xlen) -1 )
				tempY= int( ( (listSamples[i])[1] +ylen +(0.5*y) )/ (2.0*ylen) -1 )
				pixelTraj=pixelTraj+ str(tempX)+","+str(308-tempY)+";"
		else:
			pixelTraj="error_planning"
	return pixelTraj,env


class myHandler(BaseHTTPRequestHandler):

	#Handler for posts (all data transfers will be done with posts)
	def do_POST(self):
		print(self.path)
		print "Received coordinates from client"
		length = int(self.headers['Content-length'])
		s=self.rfile.read(length)
		print "I received a string: " + str (s)		
		#Convert received string into a list of tuples
		L=urlparse.parse_qsl(s)
		print "This is after parsing: "+str(L)

		if (len(L)<=3):
			print "Did not receive coordinates"
			self.send_response(200, "OK")
			self.end_headers()
			self.wfile.write("error_NoCoord") # Tells rails to render the error page
			return

		coordList=[int((L[0])[1]), int((L[1])[1]), int((L[2])[1]), int((L[3])[1])] #x1,y1,x2,y2
		envNum = (L[4])[1]
		print "Envirnoment number: "+envNum
		#Call openrave function
		pixelTraj,env=main(envNum,coordList)
		env.Destroy()

		if (pixelTraj=="error_planning"):
			print "Unable to plan a path"
			self.send_response(200, "OK")
			self.end_headers()
			self.wfile.write("error") # Tells rails to render the error page

		elif (pixelTraj=="error_Collision"):
			print "Collision error with initial coordinates"
			self.send_response(200, "OK")
			self.end_headers()
			self.wfile.write("error_Collision") # Tells rails to render the error page			
		else:
			print "These are the pixelTraj"
			print pixelTraj
			self.send_response(200, "OK")
			self.end_headers()
			self.wfile.write(pixelTraj)


if __name__ == "__main__":
	PORT_NUMBER = 8080
	try:
		#Create a web server and define the handler to manage the
		#incoming request
		server = HTTPServer(('', PORT_NUMBER), myHandler)
		print 'Started httpserver on port ' , PORT_NUMBER
		
		#Wait forever for incoming http requests
		server.serve_forever()

	except KeyboardInterrupt:
		print '^C received, shutting down the web server'
		server.socket.close()
