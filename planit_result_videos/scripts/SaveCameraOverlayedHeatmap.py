import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import sys
import scipy
import pickle
import os
import time
import openravepy
import numpy.random as rand
if not __openravepy_build_doc__:
    from openravepy import *
    from numpy import *
import numpy as np
import RecordTrajectory as rec
from optparse import OptionParser
from openravepy.misc import OpenRAVEGlobalArguments

global home
home = os.path.expanduser('~')

def main(env,options):
	path_env = home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/environment/'
	path = home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/overlay_heatmap/'
	env_list = range(1,21)
	use_beta = True
	
	if len(sys.argv) == 2:
		env_list = [int(sys.argv[1])]
	
	for e in env_list:
		env_path = path_env + 'env_{0}_context_{1}.dae'.format(e,1)
		if not os.path.exists(env_path):
			continue
		print "########Environment={0}############".format(e)
		env.Load(env_path)
		colorhumans(env.GetBodies())	
		camera_image_file = path + 'camera_image_env_{0}_context_{1}_beta_{2}.jpg'.format(e,1,use_beta)
		viewer = env.GetViewer()
		#tt = np.array([[1,0,0,0],[0,-1,0,0],[0,0,-1,0],[0,0,0,1]])
		file_exists = '1'
		loadheatmap(env,e,use_beta)
		raw_input("Set camera angle and press enter\n")
		I = env.GetViewer().GetCameraImage(640,480,env.GetViewer().GetCameraTransform(),[640,640,320,240])
		scipy.misc.imsave(camera_image_file,I)
		deleteallbodies(env.GetBodies())

def loadheatmap(env,env_num,use_beta):
	w1 = env.GetKinBody('Wall-1')
	w3 = env.GetKinBody('Wall-3')
	xdim = w3.GetLinks()[0].GetGeometries()[0].GetBoxExtents()[0]
	ydim = w1.GetLinks()[0].GetGeometries()[0].GetBoxExtents()[1]
	img=mpimg.imread('../figs/env_{0}_context_1_beta_{1}_noborder_small.png'.format(env_num,use_beta))
	M = len(img)
	N = len(img[0])
	X = 2.0*xdim
	Y = 2.0*ydim
	xlen = X*0.5/N
	ylen = Y*0.5/M
	kinbodies = []
	for i in range(M):
		for j in range(N):
			xcenter = (j+1)*2.0*xlen - xlen - (0.5*X)
			ycenter = (0.5*Y) - ( (i+1)*2.0*ylen - ylen)
			kinbodies.append(RaveCreateKinBody(env,''))
			kinbodies[-1].InitFromBoxes(numpy.array([[xcenter,ycenter,0,xlen,ylen,0.01]]),True)
			kiname = str(i)+"-"+str(j)
			kinbodies[-1].SetName(kiname)
			env.AddKinBody(kinbodies[-1],True)
			kinbodies[-1].GetLinks()[0].GetGeometries()[0].SetDiffuseColor(list(img[i,j])[:-1])




def deleteallbodies(bodies):
	for body in bodies:
		with env:
			env.RemoveKinBody(body)

def colorhumans(bodies):
	colors = [(1.0,0.0,0.0), (0.0,1.0,0.0), (1.0,0.5,0.1), (1.0,1.0,0.0), (1.0,10.2,0.8), (0.0,1.0,1.0), (0.5,0.2,0.5)]
	count = 0
	robot = env.GetRobots()[0]
	env.RemoveKinBody(robot)
	for body in bodies:	
		if body.GetName().startswith('human'):
			body.GetLinks()[0].GetGeometries()[0].SetDiffuseColor(colors[count])
			count += 1

@openravepy.with_destroy
def run(args=None):
    global env
    parser = OptionParser(description='Explicitly specify goals to get a simple navigation and manipulation demo.', usage='openrave.py --example hanoi [options]')
    OpenRAVEGlobalArguments.addOptions(parser)
    parser.add_option('--planner',action="store",type='string',dest='planner',default=None,help='the planner to use')
    (options, leftargs) = parser.parse_args(args=args)
    env = OpenRAVEGlobalArguments.parseAndCreate(options,defaultviewer=True)
    main(env,options)
					    
if __name__ == "__main__":
	run()
