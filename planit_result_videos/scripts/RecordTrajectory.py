import pickle
import os
import time
import openravepy
import numpy.random as rand
if not __openravepy_build_doc__:
    from openravepy import *
import sys
import os
import numpy as np

from optparse import OptionParser
from openravepy.misc import OpenRAVEGlobalArguments

global home
home = os.path.expanduser('~')

def waitrobot(robot):
	"""busy wait for robot completion"""
	while not robot.GetController().IsDone():
		time.sleep(0.01)

def move_arm(openrave_traj,env,robot):
	traj = RaveCreateTrajectory(env,'')
	traj.deserialize(openrave_traj)
	robot.GetController().SetPath(traj)
	waitrobot(robot)
	return

def gotoStartLocation(openrave_traj):
	traj = RaveCreateTrajectory(env,'')
	traj.deserialize(openrave_traj)

	traj_temp = RaveCreateTrajectory(env,'')
	traj_temp.Init(traj.GetConfigurationSpecification())
	traj_temp.Insert(0,traj.GetWaypoint(0),traj.GetConfigurationSpecification(),True)
	robot.GetController().SetPath(traj_temp)
	waitrobot(robot) 
	return

def Playtraj(traj_path,env):
	robot = env.GetRobots()[0]
	with open(traj_path,'rb') as ff:
		trajs = pickle.load(ff)	
	for traj in trajs:
		move_arm(traj,env,robot)

def main(env,options):
	global robot
	path = '../environment/' #home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/environment/'
	recorder = RaveCreateModule(env,'viewerrecorder')
	env.AddModule(recorder,'')
	codecs = recorder.SendCommand('GetCodecs')
	codec = 13
	
	env_list = range(1,21)
	
	if len(sys.argv) == 2:
		env_list = [int(sys.argv[1])]

	for e in env_list:
		env_path = path + 'env_{0}_context_{1}.dae'.format(e,1)
		traj_path = path + 'traj_env_{0}_context_{1}_id_1.pik'.format(e,1)
		camera_angle_path = path + 'camera_angle_env_{0}_context_1.pik'.format(e,1)
		video_file = path + '../videos/video_env_{0}_context_1.mp4'.format(e,1)
		video_file_h264 = path + '../videos/video_env_{0}_context_1_h264.mp4'.format(e,1)
		if not (os.path.exists(env_path) and os.path.exists(traj_path) and os.path.exists(camera_angle_path)):
			continue
		env.Load(env_path)
		#colorfloorandwall(e)
		colorhumans(env.GetBodies())
		robot = env.GetRobots()[0]
		with open(traj_path,'rb') as ff:
			trajs = pickle.load(ff)	
		with open(camera_angle_path,'rb') as ff:
			camera_angle = pickle.load(ff)
		viewer = env.GetViewer()
		viewer.SetCamera(camera_angle)
		gotoStartLocation(trajs[0])
		while os.path.exists(video_file):
			os.system('rm -rf {0}'.format(video_file))
			os.system('rm -rf {0}'.format(video_file_h264))
		time.sleep(1)

		recorder.SendCommand('Start 640 480 30 codec %d filename %s\nviewer %s'%(codec,video_file,env.GetViewer().GetName()))
		Playtraj(traj_path,env)
		recorder.SendCommand('Stop')	
		os.system('ffmpeg -i {0} -c:v libx264 -preset slow -crf 22 -c:a copy {1}'.format(video_file,video_file_h264))
		deleteallbodies(env.GetBodies())

def colorfloorandwall(env_num):
	#home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/environment/master_color'
	color_file = open('../environment/master_color','r')
	lines = color_file.readlines()
	line = lines[env_num-1].strip().split(';')
	color_val = line[1]
	wall_color = [float(x) for x in color_val.split(',')]
	wallnames = ['Wall-1','Wall-2','Wall-3','Wall-4']
        floor_color = [0.5,0.5,0.5]     
	if int(env_num) > 15:
		wallnames = ['wall-1','wall-2','wall-3','wall-4']
		env.GetKinBody('floor').GetLinks()[0].GetGeometries()[0].SetDiffuseColor(floor_color)
	else:
		env.GetKinBody('Floor').GetLinks()[0].GetGeometries()[0].SetDiffuseColor(floor_color)
        for walls in wallnames:
		env.GetKinBody(walls).GetLinks()[0].GetGeometries()[0].SetDiffuseColor(wall_color)

def deleteallbodies(bodies):
	for body in bodies:
		with env:
			env.RemoveKinBody(body)

def colorhumans(bodies):
	colors = [(1.0,0.0,0.0), (0.0,1.0,0.0), (1.0,0.5,0.1), (1.0,1.0,0.0), (1.0,10.2,0.8), (0.0,1.0,1.0), (0.5,0.2,0.5)]
	count = 0
	for body in bodies:	
		if body.GetName().startswith('human'):
			body.GetLinks()[0].GetGeometries()[0].SetDiffuseColor(colors[count])
			count += 1

@openravepy.with_destroy
def run(args=None):
    global env
    parser = OptionParser(description='Explicitly specify goals to get a simple navigation and manipulation demo.', usage='openrave.py --example hanoi [options]')
    OpenRAVEGlobalArguments.addOptions(parser)
    parser.add_option('--planner',action="store",type='string',dest='planner',default=None,help='the planner to use')
    (options, leftargs) = parser.parse_args(args=args)
    env = OpenRAVEGlobalArguments.parseAndCreate(options,defaultviewer=True)
    main(env,options)
					    
if __name__ == "__main__":
	run()
