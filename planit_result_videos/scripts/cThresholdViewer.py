"""Alternative planner to a.py; Connects with Plan-It rails server
"""
from __future__ import with_statement # for python 2.5
__author__= 'Ashesh Jain, Daniel Liu'
import os
import time
import openravepy
import numpy.random as rand
if not __openravepy_build_doc__:
    from openravepy import *
    from numpy import *
import xml.dom.minidom
import sys
sys.path.append('../evaluationcode/')
import plotheatmap as pm
import os
import numpy as np
from optparse import OptionParser
from openravepy.misc import OpenRAVEGlobalArguments
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import urlparse

#xyDim is robot env dim, imageXY is image dim,  x1,y1 is pixel coord
def PixelToRobot(xdim,ydim, x1, y1, imageX, imageY):	
	M = imageX #image pixel size
	N = imageY #image pixel size
	X = 2.0*xdim
	Y = 2.0*ydim
	xlen = X*0.5/N
	ylen = Y*0.5/M
	xcenter = (x1+1)*2.0*xlen - xlen - (0.5*X)   #Robot coordinates
	ycenter = (0.5*Y) - ( (y1+1)*2.0*ylen - ylen)
	return xcenter, ycenter

#Only calc xlen,ylen
def xylen(xdim,ydim,imageX, imageY):
	M = imageX #image pixel size
	N = imageY #image pixel size
	X = 2.0*xdim
	Y = 2.0*ydim
	xlen = X*0.5/N
	ylen = Y*0.5/M
	return xlen, ylen

def addObstacle(env,x,y):
	'''
	adds an Obstacle at the x,y location
	'''
	global obstacle_count
	with env:
		obstacle = RaveCreateKinBody(env,'')
		obstacle.SetName('obstacle-'+str(obstacle_count))
		obstacle.InitFromBoxes(numpy.array([[x, y, 0.1 ,0.10,0.10,0.1]]),True)
		env.Add(obstacle,True)
		obstacle_count += 1	

def deleteAllObstacles(env):
	'''
	Description: This function deletes all the obstacles
	'''
	for body in env.GetBodies():
		kinname = body.GetName()
		first_name = kinname.split('-')[0]
		if first_name == 'obstacle':
			with env:
				env.Remove(body)
	with env:
		env.UpdatePublishedBodies()	    	

#Heatmap Array is 80x80; Image on web is 308x308
def main():
	#envList=["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"]
	envList=["8"]
	for num in envList:
		envNum=num
		env_colladafile = '../environment/env_'+envNum+'_context_1.dae'
		env = Environment() # create the environment
		env.SetViewer('qtcoin') # start the viewer; Comment out if using multiple environments
		env.Load(env_colladafile)
		robot = env.GetRobots()[0] # get the first robot
		global obstacle_count
		obstacle_count=0
		env.UpdatePublishedBodies()
		time.sleep(0.1)

		manipprob = interfaces.BaseManipulation(robot) # create the interface for basic manipulation programs
		with env:
			robot.SetActiveDOFs([],DOFAffine.X|DOFAffine.Y|DOFAffine.RotationAxis,[0,0,1]) #moves the base
			w1 = env.GetKinBody('Wall-1')
			w3 = env.GetKinBody('Wall-3')
			xdim = w3.GetLinks()[0].GetGeometries()[0].GetBoxExtents()[0]
			ydim = w1.GetLinks()[0].GetGeometries()[0].GetBoxExtents()[1]
			
			maxThreshEnv=[0.468130093932,0.625690434246,0.697627020728,1.39128673944,1.2367608488,0.411941447636,1.2192291683,0.60350522026,0.392524259399,1.37457149852,0.512757512297,0.496851952358,1.25210994328,0.282127061375,0.248282652797]
			maxThresh=maxThreshEnv[int(envNum)-1]
			lowThresh=maxThresh/4
			step=(maxThresh-lowThresh)/3	
			step=step*2

			#Add Obstacles
			array=np.load("../evaluationcode/env"+envNum+".npy")
			for row in range(0,80,4):
				for col in range(0,80,4):
					if (array[row][col]>(lowThresh+step)):
						x,y=PixelToRobot(xdim,ydim,col,row,80,80)
						addObstacle(env,x,y)
			env.UpdatePublishedBodies()	    
			print envNum+": "+str(obstacle_count)
	


if __name__ == "__main__":
	main()

