import os
import sys
import numpy as np

global home, rstart, rend
home = os.path.expanduser('~')
rstart = 1
rend = 19

def copy_video_files():
	path =  home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/'

	video_path = path + 'videos/'
	copyto_location = path + '../planning_videos/planning_videos/videos/planit_videos/'

	for i in range(rstart,rend):
		file_from = video_path + 'video_env_{0}_context_1_h264.mp4'.format(i)
		os.system('mkdir -p {0}'.format(copyto_location+str(i)))
		file_to = copyto_location + '{0}/video.mp4'.format(i)
		os.system('sudo cp {0} {1}'.format(file_from,file_to))

def copy_camera_images():
	path =  home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/'

	camera_images = path + 'environment/'
	copyto_location = path + '../planning_videos/planning_videos/videos/planit_videos/'

	for i in range(rstart,rend):
		file_from = camera_images + 'camera_image_env_{0}_context_1.jpg'.format(i)
		file_to = copyto_location + '{0}/scene_2_c.png'.format(i)
		file_trim = copyto_location + '{0}/scene_2_tr_c.png'.format(i)
		file_for_seeit = copyto_location + '{0}/scene_2_tr_c_seeit.png'.format(i)
		file_trim_lc = copyto_location + '{0}/scene_2_tr_c_16color.png'.format(i)
		file_for_seeit_lc = copyto_location + '{0}/scene_2_tr_c_seeit_16color.png'.format(i)
		#os.system('sudo cp {0} {1}'.format(file_from,file_to))
		os.system('sudo convert {0} -fuzz 10% -trim -transparent white {1}'.format(file_from,file_to))
		os.system('sudo convert {0} -fuzz 10% -resize 50% {1}'.format(file_to,file_trim))
		os.system('sudo convert {0} -resize 30% {1}'.format(file_to,file_for_seeit))
		os.system('sudo convert {0} -colors 16 {1}'.format(file_trim,file_trim_lc))
		os.system('sudo convert {0} -colors 16 {1}'.format(file_for_seeit,file_for_seeit_lc))
		file_from = camera_images + '{0}_context_1.xml'.format(i)
		file_to = copyto_location + '{0}/context.txt'.format(i)
		os.system('sudo cp {0} {1}'.format(file_from,file_to))

def copy_overlay_heatmaps():
	path =  home + '/project/robotics/data/tasks_vaibhav/planit_result_videos/'

	camera_images = path + 'overlay_heatmap/'
	copyto_location = path + '../planning_videos/planning_videos/videos/planit_videos/'

	for i in range(rstart,rend):
		file_from = camera_images + 'camera_image_env_{0}_context_1_beta_True.jpg'.format(i)
		file_to = copyto_location + '{0}/scene_heatmap.png'.format(i)
		file_trim = copyto_location + '{0}/scene_heatmap_tr.png'.format(i)
		#os.system('sudo cp {0} {1}'.format(file_from,file_to))
		os.system('sudo convert {0} -fuzz 10% -transparent white {1}'.format(file_from,file_to))
		os.system('sudo convert {0} -fuzz 10% -trim {1}'.format(file_to,file_trim))

if __name__=="__main__":
	copy_video_files()
	copy_camera_images()
	copy_overlay_heatmaps()
