from __future__ import with_statement # for python 2.5
__author__= 'Ashesh Jain, Daniel Liu'
import os
import time
import openravepy
import numpy.random as rand
if not __openravepy_build_doc__:
    from openravepy import *
    from numpy import *
import xml.dom.minidom
import sys
sys.path.append('../evaluationcode/')
import plotheatmap as pm
import os
import numpy as np
from optparse import OptionParser
from openravepy.misc import OpenRAVEGlobalArguments
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import urlparse

def waitrobot(robot):
	"""busy wait for robot completion"""
	while not robot.GetController().IsDone():
		time.sleep(0.01)

def main(env,options):
	env_colladafile = sys.argv[1]
	list_traj = planmanytraj(env_colladafile,env)
	
def filler(envNum,coordList):
	'''
	This filler method is only for testing purpose. You may ignore it. 
	'''
	env_colladafile = '../environment/env_'+envNum+'_context_1.dae'
	context_graph = '../environment/'+envNum+'_graph_1.xml'
	if (envNum<=8):
		params_file = '../params/params_task_bedroom_env_1,2,3,4,5,6,7,8,9_context_1,2,3,4_s_False_r_False_n_False_b_True.pik'
	else:
		params_file = '../params/params_task_entertainment_env_1,2,3,4,5,6,7,8_context_1,2,3,4_s_False_r_False_n_False_b_True.pik'
	listSamples,xlen,ylen=FindBestTraj(env_colladafile,context_graph,params_file, coordList)
	return listSamples,xlen,ylen
'''
Mostly this will be the entering function.
'''
def FindBestTraj(env_colladafile,context_graph,params_file, coordList):
	'''
	Input: Environment colladafile, context graph file and learned params pickle file
	Output: Executes top k trajectories
	Description: It samples many trajectories using planmanytraj() function and ranks
	them using the learned params.
	'''
	scores = []
	list_traj,env, xlen, ylen = planmanytraj(env_colladafile, coordList)
	if list_traj=="error_Collision":
		listSamples="error_Collision"
		xlen=0
		ylen=0
		env.Destroy()
		return listSamples,xlen,ylen

	list_traj = np.array(list_traj)
	for traj in list_traj:
		score = pm.scoresingletraj(env_colladafile,context_graph,params_file,traj,use_beta=True)
		scores.append(score)
	scores = np.array(scores)
	sorted_args = np.argsort(scores)
	sorted_list_traj = list_traj[sorted_args]
	sorted_scores = scores[sorted_args]
	print "Scores"
	print sorted_scores
	listSamples=executeTopKtraj(env,env_colladafile,sorted_list_traj,1)
	env.Destroy()
	return listSamples, xlen, ylen

def executeTopKtraj(env,env_colladafile,list_traj,k=1):
	'''
	Input: Environment colladafile and a list of trajectories
	Output: It executes top k trajectories from the list
	Description: It first loads the environment from colladafile and then execute the trajectories.
	'''
	k = min([k,len(list_traj)])
	listSamples=executeAllTraj(env,list_traj[:k])
	return listSamples

def planmanytraj(env_colladafile, coordList, env=None):
	'''
	Input: An environment pointer and path to the environment dae file to be loaded
	Output: Many diverse trajectories
	Description: It ask user for robot start and end configuration and then plans 
	many diverse trajectories between the two configurations. To produce diverse 
	trajectories it plans a trajectory and then updates the environment by blocking 
	the trajectory by introducing an artificial obstacle. This forces next trajectory
	to be different.
	'''
	global obstacle_count
	obstacle_count = 0
	if env is None:
		env = Environment()		
		#Setting the viewer causes openrave to seg-fault on repeated calls
		#env.SetViewer('qtcoin')
	env.Load(env_colladafile)
	robot = env.GetRobots()[0]

	#Map pixel coordinates to robot coordinates
	w1 = env.GetKinBody('Wall-1')
	w3 = env.GetKinBody('Wall-3')
	xdim = w3.GetLinks()[0].GetGeometries()[0].GetBoxExtents()[0]
	ydim = w1.GetLinks()[0].GetGeometries()[0].GetBoxExtents()[1]
	M = 308 #image pixel size
	N = 308 #image pixel size
	X = 2.0*xdim
	Y = 2.0*ydim
	xlen = X*0.5/N
	ylen = Y*0.5/M

	j=coordList[0]  #x
	i=coordList[1]  #y
	
	xcenter = (j+1)*2.0*xlen - xlen - (0.5*X)   #Robot coordinates
	ycenter = (0.5*Y) - ( (i+1)*2.0*ylen - ylen)

	j=coordList[2]  #x
	i=coordList[3]  #y
	
	xcenter2 = (j+1)*2.0*xlen - xlen - (0.5*X)
	ycenter2 = (0.5*Y) - ( (i+1)*2.0*ylen - ylen)

	start_config=[[0,1,0,xcenter],[-1,0,-0,ycenter],[-0,0,1,0.04526],[0,0,0,1]]
	end_config=[[0,1,0,xcenter2],[-1,0,-0,ycenter2],[-0,0,1,0.04526],[0,0,0,1]]
	
	#Check for inital collisions
	with env:
		with robot:
			robot.SetActiveDOFs([],DOFAffine.X|DOFAffine.Y|DOFAffine.RotationAxis,[0,0,1]) #moves the base
			robot.SetActiveDOFValues([xcenter,ycenter,0])
			if env.CheckCollision(robot):
				list_traj="error_Collision"
				env.Destroy()
				return list_traj,env,xlen,ylen
			robot.SetActiveDOFValues([xcenter2,ycenter2,0])
			if env.CheckCollision(robot):
				list_traj="error_Collision"
				return list_traj,env,xlen,ylen				
	#Plan many trajectories
	list_traj = []
	for fact in [0.5]:
		with env:
			env.UpdatePublishedBodies()	    	
		for i in range(100):
			traj = plantraj(env,start_config,end_config)
			if traj is not None:
				list_traj.append(traj)
				env = addObstacle(env,traj,fact)
			else:
				env = deleteAllObstacles(env)
				break
	return list_traj,env, xlen, ylen

def deleteAllObstacles(env):
	'''
	Description: This function deletes all the obstacles
	'''
	for body in env.GetBodies():
		kinname = body.GetName()
		first_name = kinname.split('-')[0]
		if first_name == 'obstacle':
			with env:
				env.Remove(body)
	with env:
		env.UpdatePublishedBodies()	    	
	return env
		
def executeAllTraj(env,list_traj):
	'''
	Input: An environment pointer and a list of trajectories
	Description: This functions plays the trajectories in the list
	'''
	with env:
		env.UpdatePublishedBodies()	    	
	robot = env.GetRobots()[0]
	listSamples=[]
	for openrave_traj in list_traj:
		traj = RaveCreateTrajectory(env,'')
		traj.deserialize(openrave_traj)
		print 'Done planning'

		starttime = time.time()
		while time.time()-starttime < traj.GetDuration():
			curtime = time.time()-starttime
			with env:
				trajdata=traj.Sample(curtime)
				tempTuple=(trajdata[0], trajdata[1], trajdata[2])
				listSamples.append(tempTuple)
			time.sleep(1) #Sample by intervals of ~1 second
	if len(listSamples)==0 :
		listSamples="error"
		return listSamples
	return listSamples

		#waitrobot(robot) 

def plantraj(env,start_config,end_config):
	'''
	Input: An environment pointer, robot start and end confguration matrices.
	Output: A trajectory in the environment.
	'''
	print "next traj"
	robot = env.GetRobots()[0]
	#ikmodel = databases.inversekinematics.InverseKinematicsModel(robot,iktype=IkParameterization.Type.Transform6D)
	#if not ikmodel.load():
	#	ikmodel.autogenerate()
	plannertype = "BiRRT"
	basemanip = interfaces.BaseManipulation(robot,plannername=plannertype)
        
	Tgoal_ = end_config
        angx = math.acos(Tgoal_[0][0])
        angy = math.acos(Tgoal_[1][0])
	if angy < 0.5*math.pi:
		ang = angx
        else:
	        ang = 2*math.pi - angx
	with env:
		robot.SetTransform(start_config)
	try:
		with env:
			robot.SetActiveDOFs([],DOFAffine.X|DOFAffine.Y|DOFAffine.RotationAxis,[0,0,1])
			traj = basemanip.MoveActiveJoints(goal=[Tgoal_[0][3],Tgoal_[1][3],ang],maxiter=10000,steplength=0.15,maxtries=2,outputtraj=True,execute=False)
		waitrobot(robot)
		planned_successfully = True
	except planning_error,e:
		print e
		traj = None
		planned_successfully = False
	return traj


def addObstacle(env,traj,fact):
	'''
	Input: environment pointer, a trajectory and a factor
	Output: environment
	Description: This function puts a tall obstacle at fact*trajectory_duration
	and returns the new environment. 
	'''
	global obstacle_count
        trajptr = RaveCreateTrajectory(Environment(),'')
        trajptr.deserialize(traj)
	traj_duration = trajptr.GetDuration()
	midwaypoint = trajptr.Sample(fact*traj_duration)

	with env:
		obstacle = RaveCreateKinBody(env,'')
		obstacle.SetName('obstacle-'+str(obstacle_count))
		obstacle.InitFromBoxes(numpy.array([[midwaypoint[0], midwaypoint[1], 0.1 ,0.01,0.01,0.1]]),True)
		env.Add(obstacle,True)
		obstacle_count += 1
		env.UpdatePublishedBodies()	    	
	return env

class myHandler(BaseHTTPRequestHandler):

	#Handler for posts (all data transfers will be done with posts)
	def do_POST(self):
		print(self.path)
		print "Received coordinates from client"
		length = int(self.headers['Content-length'])

		#Receive a form in string format
		s=self.rfile.read(length)
		print "I received a string: " + str (s)		
		#Convert into a list of tuples
		L=urlparse.parse_qsl(s)
		print "This is after parsing: "+str(L)

		if (len(L)<=3):
			print "Did not receive coordinates"
			self.send_response(200, "OK")
			self.end_headers()
			self.wfile.write("error_NoCoord") # Tells rails to render the error page
			return

		coordList=[int((L[0])[1]), int((L[1])[1]), int((L[2])[1]), int((L[3])[1])] #x1,y1,x2,y2
		envNum = (L[4])[1]
		print "Envirnoment number: "+envNum
		#Call openrave function
		listSamples,xlen,ylen=filler(envNum,coordList)

		if (listSamples=="error"):
			print "Some sort of planning error"
			self.send_response(200, "OK")
			self.end_headers()
			self.wfile.write("error") # Tells rails to render the error page

		elif (listSamples=="error_Collision"):
			print "Collision error with initial coordinates"
			self.send_response(200, "OK")
			self.end_headers()
			self.wfile.write("error_Collision") # Tells rails to render the error page			
		else:
			print "These are the trajectory tuples: (x,y,rotation)"
			print listSamples
			#Convert to pixel coordinates BEFORE sending it back: x1,y1;x2,y2;.....etc
			pixelTraj=""

			x=(xlen*308)/0.5
			y=(ylen*308)/0.5
			for i in range (len(listSamples)):
				tempX= int( ( (listSamples[i])[0] +xlen +(0.5*x) )/ (2.0*xlen) -1 )
				tempY= int( ( (listSamples[i])[1] +ylen +(0.5*y) )/ (2.0*ylen) -1 )
				pixelTraj=pixelTraj+ str(tempX)+","+str(308-tempY)+";"
			
			self.send_response(200, "OK")
			self.end_headers()
			self.wfile.write(pixelTraj)



@openravepy.with_destroy
def run(args=None):
	global env
	parser = OptionParser(description='Explicitly specify goals to get a simple navigation and manipulation demo.', usage='openrave.py --example hanoi [options]')
	OpenRAVEGlobalArguments.addOptions(parser)
	parser.add_option('--planner',action="store",type='string',dest='planner',default=None,help='the planner to use')
	(options, leftargs) = parser.parse_args(args=args)
	env = OpenRAVEGlobalArguments.parseAndCreate(options,defaultviewer=True)
	main(env,options)
	time.sleep(4)

if __name__ == "__main__":
	PORT_NUMBER = 8080
	try:
		#Create a web server and define the handler to manage the
		#incoming request
		server = HTTPServer(('', PORT_NUMBER), myHandler)
		print 'Started httpserver on port ' , PORT_NUMBER
		
		#Wait forever for incoming htto requests
		server.serve_forever()

	except KeyboardInterrupt:
		print '^C received, shutting down the web server'
		server.socket.close()
